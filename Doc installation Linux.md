# Déployer Send
## Installer Docker
On ajoute le repo docker
```bash
[christopher@fileshare send]$ sudo dnf install -y yum-utils
Last metadata expiration check: 1:47:11 ago on Sat 15 Jan 2022 02:40:44 PM CET.
Dependencies resolved.
==============================================================================================================================================================================================================
 Package                                           Architecture                                   Version                                                Repository                                      Size
==============================================================================================================================================================================================================
Installing:
 yum-utils                                         noarch                                         4.0.21-3.el8                                           baseos                                          71 k

Transaction Summary
==============================================================================================================================================================================================================
Install  1 Package

Total download size: 71 k
Installed size: 22 k
Downloading Packages:
yum-utils-4.0.21-3.el8.noarch.rpm                                                                                                                                             190 kB/s |  71 kB     00:00
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                                                                                         120 kB/s |  71 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                                                                                                      1/1
  Installing       : yum-utils-4.0.21-3.el8.noarch                                                                                                                                                        1/1
  Running scriptlet: yum-utils-4.0.21-3.el8.noarch                                                                                                                                                        1/1
  Verifying        : yum-utils-4.0.21-3.el8.noarch                                                                                                                                                        1/1

Installed:
  yum-utils-4.0.21-3.el8.noarch

Complete!
```
```bash
[christopher@fileshare send]$ sudo yum-config-manager \
> --add-repo \
> https://download.docker.com/linux/centos/docker-ce.repo
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo
```
On télécharge et installe docker
```bash
[christopher@fileshare send]$ sudo dnf install docker-ce docker-ce-cli containerd.io
Docker CE Stable - x86_64                                                                                                                                                     100 kB/s |  19 kB     00:00
Dependencies resolved.
==============================================================================================================================================================================================================
 Package                                                Architecture                     Version                                                             Repository                                  Size
==============================================================================================================================================================================================================
Installing:
 containerd.io                                          x86_64                           1.4.12-3.1.el8                                                      docker-ce-stable                            28 M
 docker-ce                                              x86_64                           3:20.10.12-3.el8                                                    docker-ce-stable                            22 M
 docker-ce-cli                                          x86_64                           1:20.10.12-3.el8                                                    docker-ce-stable                            30 M
Installing dependencies:
 checkpolicy                                            x86_64                           2.9-1.el8                                                           baseos                                     345 k
 container-selinux                                      noarch                           2:2.167.0-1.module+el8.5.0+710+4c471e88                             appstream                                   53 k
 docker-ce-rootless-extras                              x86_64                           20.10.12-3.el8                                                      docker-ce-stable                           4.6 M
 docker-scan-plugin                                     x86_64                           0.12.0-3.el8                                                        docker-ce-stable                           3.7 M
 fuse-common                                            x86_64                           3.2.1-12.el8                                                        baseos                                      20 k
 fuse-overlayfs                                         x86_64                           1.7.1-1.module+el8.5.0+710+4c471e88                                 appstream                                   71 k
 fuse3                                                  x86_64                           3.2.1-12.el8                                                        baseos                                      49 k
 fuse3-libs                                             x86_64                           3.2.1-12.el8                                                        baseos                                      93 k
 libcgroup                                              x86_64                           0.41-19.el8                                                         baseos                                      69 k
 libslirp                                               x86_64                           4.4.0-1.module+el8.5.0+710+4c471e88                                 appstream                                   69 k
 policycoreutils-python-utils                           noarch                           2.9-16.el8                                                          baseos                                     251 k
 python3-audit                                          x86_64                           3.0-0.17.20191104git1c2f876.el8.1                                   baseos                                      85 k
 python3-libsemanage                                    x86_64                           2.9-6.el8                                                           baseos                                     126 k
 python3-policycoreutils                                noarch                           2.9-16.el8                                                          baseos                                     2.2 M
 python3-setools                                        x86_64                           4.3.0-2.el8                                                         baseos                                     625 k
 slirp4netns                                            x86_64                           1.1.8-1.module+el8.5.0+710+4c471e88                                 appstream                                   50 k
Enabling module streams:
 container-tools                                                                         rhel8

Transaction Summary
==============================================================================================================================================================================================================
Install  19 Packages

Total download size: 93 M
Installed size: 388 M
Is this ok [y/N]: y
Downloading Packages:
(1/19): container-selinux-2.167.0-1.module+el8.5.0+710+4c471e88.noarch.rpm                                                                                                    349 kB/s |  53 kB     00:00
(2/19): libslirp-4.4.0-1.module+el8.5.0+710+4c471e88.x86_64.rpm                                                                                                               453 kB/s |  69 kB     00:00
(3/19): fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64.rpm                                                                                                         410 kB/s |  71 kB     00:00
(4/19): fuse-common-3.2.1-12.el8.x86_64.rpm                                                                                                                                   221 kB/s |  20 kB     00:00
(5/19): checkpolicy-2.9-1.el8.x86_64.rpm                                                                                                                                      2.4 MB/s | 345 kB     00:00
(6/19): fuse3-3.2.1-12.el8.x86_64.rpm                                                                                                                                         968 kB/s |  49 kB     00:00
(7/19): fuse3-libs-3.2.1-12.el8.x86_64.rpm                                                                                                                                    2.0 MB/s |  93 kB     00:00
(8/19): libcgroup-0.41-19.el8.x86_64.rpm                                                                                                                                      967 kB/s |  69 kB     00:00
(9/19): python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64.rpm                                                                                                            1.2 MB/s |  85 kB     00:00
(10/19): policycoreutils-python-utils-2.9-16.el8.noarch.rpm                                                                                                                   1.1 MB/s | 251 kB     00:00
(11/19): python3-libsemanage-2.9-6.el8.x86_64.rpm                                                                                                                             785 kB/s | 126 kB     00:00
(12/19): python3-policycoreutils-2.9-16.el8.noarch.rpm                                                                                                                         20 MB/s | 2.2 MB     00:00
(13/19): slirp4netns-1.1.8-1.module+el8.5.0+710+4c471e88.x86_64.rpm                                                                                                            89 kB/s |  50 kB     00:00
(14/19): python3-setools-4.3.0-2.el8.x86_64.rpm                                                                                                                               1.5 MB/s | 625 kB     00:00
(15/19): docker-ce-20.10.12-3.el8.x86_64.rpm                                                                                                                                   38 MB/s |  22 MB     00:00
(16/19): containerd.io-1.4.12-3.1.el8.x86_64.rpm                                                                                                                               34 MB/s |  28 MB     00:00
(17/19): docker-ce-rootless-extras-20.10.12-3.el8.x86_64.rpm                                                                                                                   20 MB/s | 4.6 MB     00:00
(18/19): docker-ce-cli-20.10.12-3.el8.x86_64.rpm                                                                                                                               36 MB/s |  30 MB     00:00
(19/19): docker-scan-plugin-0.12.0-3.el8.x86_64.rpm                                                                                                                            10 MB/s | 3.7 MB     00:00
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                                                                                          40 MB/s |  93 MB     00:02
Docker CE Stable - x86_64                                                                                                                                                      16 kB/s | 1.6 kB     00:00
Importing GPG key 0x621E9F35:
 Userid     : "Docker Release (CE rpm) <docker@docker.com>"
 Fingerprint: 060A 61C5 1B55 8A7F 742B 77AA C52F EB6B 621E 9F35
 From       : https://download.docker.com/linux/centos/gpg
Is this ok [y/N]:  sudo systemctl start docker
Is this ok [y/N]: y
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                                                                                                      1/1
  Installing       : docker-scan-plugin-0.12.0-3.el8.x86_64                                                                                                                                              1/19
  Running scriptlet: docker-scan-plugin-0.12.0-3.el8.x86_64                                                                                                                                              1/19
  Installing       : docker-ce-cli-1:20.10.12-3.el8.x86_64                                                                                                                                               2/19
  Running scriptlet: docker-ce-cli-1:20.10.12-3.el8.x86_64                                                                                                                                               2/19
  Installing       : python3-setools-4.3.0-2.el8.x86_64                                                                                                                                                  3/19
  Installing       : python3-libsemanage-2.9-6.el8.x86_64                                                                                                                                                4/19
  Installing       : python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64                                                                                                                              5/19
  Running scriptlet: libcgroup-0.41-19.el8.x86_64                                                                                                                                                        6/19
  Installing       : libcgroup-0.41-19.el8.x86_64                                                                                                                                                        6/19
  Running scriptlet: libcgroup-0.41-19.el8.x86_64                                                                                                                                                        6/19
  Installing       : fuse3-libs-3.2.1-12.el8.x86_64                                                                                                                                                      7/19
  Running scriptlet: fuse3-libs-3.2.1-12.el8.x86_64                                                                                                                                                      7/19
  Installing       : fuse-common-3.2.1-12.el8.x86_64                                                                                                                                                     8/19
  Installing       : fuse3-3.2.1-12.el8.x86_64                                                                                                                                                           9/19
  Installing       : fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64                                                                                                                          10/19
  Running scriptlet: fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64                                                                                                                          10/19
  Installing       : checkpolicy-2.9-1.el8.x86_64                                                                                                                                                       11/19
  Installing       : python3-policycoreutils-2.9-16.el8.noarch                                                                                                                                          12/19
  Installing       : policycoreutils-python-utils-2.9-16.el8.noarch                                                                                                                                     13/19
  Running scriptlet: container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                                                                                                   14/19
  Installing       : container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                                                                                                   14/19
  Running scriptlet: container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                                                                                                   14/19
  Installing       : containerd.io-1.4.12-3.1.el8.x86_64                                                                                                                                                15/19
  Running scriptlet: containerd.io-1.4.12-3.1.el8.x86_64                                                                                                                                                15/19
  Installing       : libslirp-4.4.0-1.module+el8.5.0+710+4c471e88.x86_64                                                                                                                                16/19
  Installing       : slirp4netns-1.1.8-1.module+el8.5.0+710+4c471e88.x86_64                                                                                                                             17/19
  Installing       : docker-ce-rootless-extras-20.10.12-3.el8.x86_64                                                                                                                                    18/19
  Running scriptlet: docker-ce-rootless-extras-20.10.12-3.el8.x86_64                                                                                                                                    18/19
  Installing       : docker-ce-3:20.10.12-3.el8.x86_64                                                                                                                                                  19/19
  Running scriptlet: docker-ce-3:20.10.12-3.el8.x86_64                                                                                                                                                  19/19
  Running scriptlet: container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                                                                                                   19/19
  Running scriptlet: docker-ce-3:20.10.12-3.el8.x86_64                                                                                                                                                  19/19
  Verifying        : container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch                                                                                                                    1/19
  Verifying        : fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64                                                                                                                           2/19
  Verifying        : libslirp-4.4.0-1.module+el8.5.0+710+4c471e88.x86_64                                                                                                                                 3/19
  Verifying        : slirp4netns-1.1.8-1.module+el8.5.0+710+4c471e88.x86_64                                                                                                                              4/19
  Verifying        : checkpolicy-2.9-1.el8.x86_64                                                                                                                                                        5/19
  Verifying        : fuse-common-3.2.1-12.el8.x86_64                                                                                                                                                     6/19
  Verifying        : fuse3-3.2.1-12.el8.x86_64                                                                                                                                                           7/19
  Verifying        : fuse3-libs-3.2.1-12.el8.x86_64                                                                                                                                                      8/19
  Verifying        : libcgroup-0.41-19.el8.x86_64                                                                                                                                                        9/19
  Verifying        : policycoreutils-python-utils-2.9-16.el8.noarch                                                                                                                                     10/19
  Verifying        : python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64                                                                                                                             11/19
  Verifying        : python3-libsemanage-2.9-6.el8.x86_64                                                                                                                                               12/19
  Verifying        : python3-policycoreutils-2.9-16.el8.noarch                                                                                                                                          13/19
  Verifying        : python3-setools-4.3.0-2.el8.x86_64                                                                                                                                                 14/19
  Verifying        : containerd.io-1.4.12-3.1.el8.x86_64                                                                                                                                                15/19
  Verifying        : docker-ce-3:20.10.12-3.el8.x86_64                                                                                                                                                  16/19
  Verifying        : docker-ce-cli-1:20.10.12-3.el8.x86_64                                                                                                                                              17/19
  Verifying        : docker-ce-rootless-extras-20.10.12-3.el8.x86_64                                                                                                                                    18/19
  Verifying        : docker-scan-plugin-0.12.0-3.el8.x86_64                                                                                                                                             19/19

Installed:
  checkpolicy-2.9-1.el8.x86_64                                    container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch          containerd.io-1.4.12-3.1.el8.x86_64
  docker-ce-3:20.10.12-3.el8.x86_64                               docker-ce-cli-1:20.10.12-3.el8.x86_64                                     docker-ce-rootless-extras-20.10.12-3.el8.x86_64
  docker-scan-plugin-0.12.0-3.el8.x86_64                          fuse-common-3.2.1-12.el8.x86_64                                           fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64
  fuse3-3.2.1-12.el8.x86_64                                       fuse3-libs-3.2.1-12.el8.x86_64                                            libcgroup-0.41-19.el8.x86_64
  libslirp-4.4.0-1.module+el8.5.0+710+4c471e88.x86_64             policycoreutils-python-utils-2.9-16.el8.noarch                            python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64
  python3-libsemanage-2.9-6.el8.x86_64                            python3-policycoreutils-2.9-16.el8.noarch                                 python3-setools-4.3.0-2.el8.x86_64
  slirp4netns-1.1.8-1.module+el8.5.0+710+4c471e88.x86_64

Complete!
```
On test notre installation

```bash
[christopher@fileshare send]$  sudo systemctl start docker
[christopher@fileshare send]$  sudo docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
2db29710123e: Pull complete
Digest: sha256:975f4b14f326b05db86e16de00144f9c12257553bba9484fed41f9b6f2257800
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

## Lancer les conteneurs docker
On télécharge l'image du conteneur send
```bash
[christopher@fileshare send]$ sudo docker pull registry.gitlab.com/timvisee/send:latest
latest: Pulling from timvisee/send
5758d4e389a3: Pull complete
65e45f2a771d: Pull complete
fe728f6388df: Pull complete
be83d68ed338: Pull complete
5512d9aec054: Pull complete
622d7d4c8a0e: Pull complete
8d7d96515ed6: Pull complete
15e4aeec8751: Pull complete
6960442ad39c: Pull complete
f32c3d9de284: Pull complete
d061ac37efc8: Pull complete
ca2f41651d6e: Pull complete
cc99be777c9b: Pull complete
5fe03c4ca0aa: Pull complete
Digest: sha256:80e404e57189746914316fb9bbaa1801c6d262a704ba76f118d20a05ecb29182
Status: Downloaded newer image for registry.gitlab.com/timvisee/send:latest
registry.gitlab.com/timvisee/send:latest
```
On crée un réseau pour que nos deux conteneurs se parlent
`docker network create send`
On lance le premier conteneur
`docker run --net=send -v $PWD/redis:/data redis-server --appendonly yes`
On lance le second conteneur
```bash
docker run --net=timviseesend -v $PWD/uploads:/var/send/uploads -p 1443:1443 \
     -e 'BASE_URL=http://send.wood.com:1443' \
     -e 'MAX_FILE_SIZE=2147483648' \
     -e 'MAX_FILES_PER_ARCHIVE=64' \
     -e 'MAX_EXPIRE_SECONDS=604800' \
     -e 'MAX_DOWNLOADS=100' \
     -e 'DOWNLOAD_COUNTS=10,1,2,5,10,15,25,50,100' \
     -e 'EXPIRE_TIMES_SECONDS=3600,86400,604800,2592000,31536000' \
     -e 'DEFAULT_DOWNLOADS=1' \
     -e 'DEFAULT_EXPIRE_SECONDS=86400' \
     registry.gitlab.com/timvisee/send:latest
 ```
 Notre send est accessible via l'URL http://send.wood.com:1443
 Il prend des fichiers de 2Gb maximum et il peut prendre 64 fichiers par transfert
 Il garde les fichiers au maximum 7 jours ou 100 téléchargements
 Il propose différent nombre de téléchargement ou temps de rétention avec comme choix par défaut "1 téléchargement ou 1 jour"
 
[En image](https://cdn.discordapp.com/attachments/576780826554007584/934030458105241600/Pasted_image_20220116165252.png)
 
 # Proxy Nginx
 ## Installer NGINX : 
```bash
[christopher@proxy ~]$ sudo dnf install nginx
[sudo] password for christopher:
Last metadata expiration check: 1:01:11 ago on Mon 13 Dec 2021 12:56:22 PM CET.
Dependencies resolved.
====================================================================================
 Package              Arch   Version                                Repo       Size
====================================================================================
Installing:
 nginx                x86_64 1:1.14.1-9.module+el8.4.0+542+81547229 appstream 566 k
Installing dependencies:
 fontconfig           x86_64 2.13.1-4.el8                           baseos    273 k
 gd                   x86_64 2.2.5-7.el8                            appstream 143 k
 jbigkit-libs         x86_64 2.1-14.el8                             appstream  54 k
 libX11               x86_64 1.6.8-5.el8                            appstream 610 k
 libX11-common        noarch 1.6.8-5.el8                            appstream 157 k
 libXau               x86_64 1.0.9-3.el8                            appstream  36 k
 libXpm               x86_64 3.5.12-8.el8                           appstream  57 k
 libjpeg-turbo        x86_64 1.5.3-12.el8                           appstream 156 k
 libtiff              x86_64 4.0.9-20.el8                           appstream 187 k
 libwebp              x86_64 1.0.0-5.el8                            appstream 271 k
 libxcb               x86_64 1.13.1-1.el8                           appstream 228 k
 libxslt              x86_64 1.1.32-6.el8                           baseos    249 k
 nginx-all-modules    noarch 1:1.14.1-9.module+el8.4.0+542+81547229 appstream  22 k
 nginx-filesystem     noarch 1:1.14.1-9.module+el8.4.0+542+81547229 appstream  23 k
 nginx-mod-http-image-filter
                      x86_64 1:1.14.1-9.module+el8.4.0+542+81547229 appstream  34 k
 nginx-mod-http-perl  x86_64 1:1.14.1-9.module+el8.4.0+542+81547229 appstream  45 k
 nginx-mod-http-xslt-filter
                      x86_64 1:1.14.1-9.module+el8.4.0+542+81547229 appstream  32 k
 nginx-mod-mail       x86_64 1:1.14.1-9.module+el8.4.0+542+81547229 appstream  63 k
 nginx-mod-stream     x86_64 1:1.14.1-9.module+el8.4.0+542+81547229 appstream  84 k
 perl-Carp            noarch 1.42-396.el8                           baseos     29 k
 perl-Data-Dumper     x86_64 2.167-399.el8                          baseos     57 k
 perl-Digest          noarch 1.17-395.el8                           appstream  26 k
 perl-Digest-MD5      x86_64 2.55-396.el8                           appstream  36 k
 perl-Encode          x86_64 4:2.97-3.el8                           baseos    1.5 M
 perl-Errno           x86_64 1.28-420.el8                           baseos     75 k
 perl-Exporter        noarch 5.72-396.el8                           baseos     33 k
 perl-File-Path       noarch 2.15-2.el8                             baseos     37 k
 perl-File-Temp       noarch 0.230.600-1.el8                        baseos     62 k
 perl-Getopt-Long     noarch 1:2.50-4.el8                           baseos     62 k
 perl-HTTP-Tiny       noarch 0.074-1.el8                            baseos     57 k
 perl-IO              x86_64 1.38-420.el8                           baseos    141 k
 perl-MIME-Base64     x86_64 3.15-396.el8                           baseos     30 k
 perl-Net-SSLeay      x86_64 1.88-1.module+el8.4.0+512+d4f0fc54     appstream 378 k
 perl-PathTools       x86_64 3.74-1.el8                             baseos     89 k
 perl-Pod-Escapes     noarch 1:1.07-395.el8                         baseos     19 k
 perl-Pod-Perldoc     noarch 3.28-396.el8                           baseos     85 k
 perl-Pod-Simple      noarch 1:3.35-395.el8                         baseos    212 k
 perl-Pod-Usage       noarch 4:1.69-395.el8                         baseos     33 k
 perl-Scalar-List-Utils
                      x86_64 3:1.49-2.el8                           baseos     67 k
 perl-Socket          x86_64 4:2.027-3.el8                          baseos     58 k
 perl-Storable        x86_64 1:3.11-3.el8                           baseos     97 k
 perl-Term-ANSIColor  noarch 4.06-396.el8                           baseos     45 k
 perl-Term-Cap        noarch 1.17-395.el8                           baseos     22 k
 perl-Text-ParseWords noarch 3.30-395.el8                           baseos     17 k
 perl-Text-Tabs+Wrap  noarch 2013.0523-395.el8                      baseos     23 k
 perl-Time-Local      noarch 1:1.280-1.el8                          baseos     32 k
 perl-URI             noarch 1.73-3.el8                             appstream 115 k
 perl-Unicode-Normalize
                      x86_64 1.25-396.el8                           baseos     81 k
 perl-constant        noarch 1.33-396.el8                           baseos     24 k
 perl-interpreter     x86_64 4:5.26.3-420.el8                       baseos    6.3 M
 perl-libnet          noarch 3.11-3.el8                             appstream 120 k
 perl-libs            x86_64 4:5.26.3-420.el8                       baseos    1.6 M
 perl-macros          x86_64 4:5.26.3-420.el8                       baseos     71 k
 perl-parent          noarch 1:0.237-1.el8                          baseos     19 k
 perl-podlators       noarch 4.11-1.el8                             baseos    117 k
 perl-threads         x86_64 1:2.21-2.el8                           baseos     60 k
 perl-threads-shared  x86_64 1.58-2.el8                             baseos     47 k
Installing weak dependencies:
 perl-IO-Socket-IP    noarch 0.39-5.el8                             appstream  46 k
 perl-IO-Socket-SSL   noarch 2.066-4.module+el8.4.0+512+d4f0fc54    appstream 297 k
 perl-Mozilla-CA      noarch 20160104-7.module+el8.4.0+529+e3b3e624 appstream  14 k
Enabling module streams:
 nginx                       1.14
 perl                        5.26
 perl-IO-Socket-SSL          2.066
 perl-libwww-perl            6.34

Transaction Summary
====================================================================================
Install  61 Packages

Total download size: 15 M
Installed size: 45 M
Is this ok [y/N]: y
Downloading Packages:
(1/61): jbigkit-libs-2.1-14.el8.x86_64.rpm          354 kB/s |  54 kB     00:00
(2/61): gd-2.2.5-7.el8.x86_64.rpm                   701 kB/s | 143 kB     00:00
(3/61): libXau-1.0.9-3.el8.x86_64.rpm               905 kB/s |  36 kB     00:00
(4/61): libXpm-3.5.12-8.el8.x86_64.rpm              1.2 MB/s |  57 kB     00:00
(5/61): libX11-1.6.8-5.el8.x86_64.rpm               2.0 MB/s | 610 kB     00:00
(6/61): libX11-common-1.6.8-5.el8.noarch.rpm        1.1 MB/s | 157 kB     00:00
(7/61): libtiff-4.0.9-20.el8.x86_64.rpm             3.3 MB/s | 187 kB     00:00
(8/61): libjpeg-turbo-1.5.3-12.el8.x86_64.rpm       2.0 MB/s | 156 kB     00:00
(9/61): libwebp-1.0.0-5.el8.x86_64.rpm              2.7 MB/s | 271 kB     00:00
(10/61): libxcb-1.13.1-1.el8.x86_64.rpm             4.0 MB/s | 228 kB     00:00
(11/61): nginx-filesystem-1.14.1-9.module+el8.4.0+5 761 kB/s |  23 kB     00:00
(12/61): nginx-all-modules-1.14.1-9.module+el8.4.0+ 526 kB/s |  22 kB     00:00
(13/61): nginx-mod-http-image-filter-1.14.1-9.modul 758 kB/s |  34 kB     00:00
(14/61): nginx-mod-http-perl-1.14.1-9.module+el8.4. 889 kB/s |  45 kB     00:00
(15/61): nginx-mod-http-xslt-filter-1.14.1-9.module 1.0 MB/s |  32 kB     00:00
(16/61): nginx-1.14.1-9.module+el8.4.0+542+81547229 3.5 MB/s | 566 kB     00:00
(17/61): nginx-mod-mail-1.14.1-9.module+el8.4.0+542 1.6 MB/s |  63 kB     00:00
(18/61): nginx-mod-stream-1.14.1-9.module+el8.4.0+5 2.2 MB/s |  84 kB     00:00
(19/61): perl-Digest-1.17-395.el8.noarch.rpm        463 kB/s |  26 kB     00:00
(20/61): perl-Digest-MD5-2.55-396.el8.x86_64.rpm    538 kB/s |  36 kB     00:00
(21/61): perl-Mozilla-CA-20160104-7.module+el8.4.0+ 358 kB/s |  14 kB     00:00
(22/61): perl-IO-Socket-IP-0.39-5.el8.noarch.rpm    533 kB/s |  46 kB     00:00
(23/61): perl-IO-Socket-SSL-2.066-4.module+el8.4.0+ 2.5 MB/s | 297 kB     00:00
(24/61): perl-URI-1.73-3.el8.noarch.rpm             884 kB/s | 115 kB     00:00
(25/61): perl-Net-SSLeay-1.88-1.module+el8.4.0+512+ 2.7 MB/s | 378 kB     00:00
(26/61): perl-libnet-3.11-3.el8.noarch.rpm          1.4 MB/s | 120 kB     00:00
(27/61): perl-Carp-1.42-396.el8.noarch.rpm          283 kB/s |  29 kB     00:00
(28/61): perl-Data-Dumper-2.167-399.el8.x86_64.rpm  760 kB/s |  57 kB     00:00
(29/61): libxslt-1.1.32-6.el8.x86_64.rpm            1.2 MB/s | 249 kB     00:00
(30/61): fontconfig-2.13.1-4.el8.x86_64.rpm         1.1 MB/s | 273 kB     00:00
(31/61): perl-Errno-1.28-420.el8.x86_64.rpm         1.3 MB/s |  75 kB     00:00
(32/61): perl-Exporter-5.72-396.el8.noarch.rpm      472 kB/s |  33 kB     00:00
(33/61): perl-File-Path-2.15-2.el8.noarch.rpm       325 kB/s |  37 kB     00:00
(34/61): perl-File-Temp-0.230.600-1.el8.noarch.rpm  428 kB/s |  62 kB     00:00
(35/61): perl-Getopt-Long-2.50-4.el8.noarch.rpm     764 kB/s |  62 kB     00:00
(36/61): perl-HTTP-Tiny-0.074-1.el8.noarch.rpm      814 kB/s |  57 kB     00:00
(37/61): perl-IO-1.38-420.el8.x86_64.rpm            1.9 MB/s | 141 kB     00:00
(38/61): perl-MIME-Base64-3.15-396.el8.x86_64.rpm   535 kB/s |  30 kB     00:00
(39/61): perl-Encode-2.97-3.el8.x86_64.rpm          3.8 MB/s | 1.5 MB     00:00
(40/61): perl-PathTools-3.74-1.el8.x86_64.rpm       1.2 MB/s |  89 kB     00:00
(41/61): perl-Pod-Escapes-1.07-395.el8.noarch.rpm   366 kB/s |  19 kB     00:00
(42/61): perl-Pod-Perldoc-3.28-396.el8.noarch.rpm   1.1 MB/s |  85 kB     00:00
(43/61): perl-Scalar-List-Utils-1.49-2.el8.x86_64.r 962 kB/s |  67 kB     00:00
(44/61): perl-Pod-Simple-3.35-395.el8.noarch.rpm    1.7 MB/s | 212 kB     00:00
(45/61): perl-Pod-Usage-1.69-395.el8.noarch.rpm     326 kB/s |  33 kB     00:00
(46/61): perl-Socket-2.027-3.el8.x86_64.rpm         949 kB/s |  58 kB     00:00
(47/61): perl-Storable-3.11-3.el8.x86_64.rpm        1.5 MB/s |  97 kB     00:00
(48/61): perl-Term-ANSIColor-4.06-396.el8.noarch.rp 659 kB/s |  45 kB     00:00
(49/61): perl-Term-Cap-1.17-395.el8.noarch.rpm      518 kB/s |  22 kB     00:00
(50/61): perl-Text-ParseWords-3.30-395.el8.noarch.r 310 kB/s |  17 kB     00:00
(51/61): perl-Text-Tabs+Wrap-2013.0523-395.el8.noar 333 kB/s |  23 kB     00:00
(52/61): perl-Time-Local-1.280-1.el8.noarch.rpm     659 kB/s |  32 kB     00:00
(53/61): perl-Unicode-Normalize-1.25-396.el8.x86_64 1.5 MB/s |  81 kB     00:00
(54/61): perl-constant-1.33-396.el8.noarch.rpm      611 kB/s |  24 kB     00:00
(55/61): perl-macros-5.26.3-420.el8.x86_64.rpm      947 kB/s |  71 kB     00:00
(56/61): perl-parent-0.237-1.el8.noarch.rpm         298 kB/s |  19 kB     00:00
(57/61): perl-podlators-4.11-1.el8.noarch.rpm       1.4 MB/s | 117 kB     00:00
(58/61): perl-threads-2.21-2.el8.x86_64.rpm         983 kB/s |  60 kB     00:00
(59/61): perl-libs-5.26.3-420.el8.x86_64.rpm        5.1 MB/s | 1.6 MB     00:00
(60/61): perl-threads-shared-1.58-2.el8.x86_64.rpm  1.1 MB/s |  47 kB     00:00
(61/61): perl-interpreter-5.26.3-420.el8.x86_64.rpm 4.5 MB/s | 6.3 MB     00:01
------------------------------------------------------------------------------------
Total                                               4.4 MB/s |  15 MB     00:03
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                            1/1
  Installing       : libjpeg-turbo-1.5.3-12.el8.x86_64                         1/61
  Installing       : perl-Digest-1.17-395.el8.noarch                           2/61
  Installing       : perl-Digest-MD5-2.55-396.el8.x86_64                       3/61
  Installing       : perl-Data-Dumper-2.167-399.el8.x86_64                     4/61
  Installing       : perl-libnet-3.11-3.el8.noarch                             5/61
  Installing       : perl-Net-SSLeay-1.88-1.module+el8.4.0+512+d4f0fc54.x86    6/61
  Installing       : perl-URI-1.73-3.el8.noarch                                7/61
  Installing       : perl-Pod-Escapes-1:1.07-395.el8.noarch                    8/61
  Installing       : perl-Mozilla-CA-20160104-7.module+el8.4.0+529+e3b3e624    9/61
  Installing       : perl-IO-Socket-IP-0.39-5.el8.noarch                      10/61
  Installing       : perl-Time-Local-1:1.280-1.el8.noarch                     11/61
  Installing       : perl-IO-Socket-SSL-2.066-4.module+el8.4.0+512+d4f0fc54   12/61
  Installing       : perl-Term-ANSIColor-4.06-396.el8.noarch                  13/61
  Installing       : perl-Term-Cap-1.17-395.el8.noarch                        14/61
  Installing       : perl-File-Temp-0.230.600-1.el8.noarch                    15/61
  Installing       : perl-Pod-Simple-1:3.35-395.el8.noarch                    16/61
  Installing       : perl-HTTP-Tiny-0.074-1.el8.noarch                        17/61
  Installing       : perl-podlators-4.11-1.el8.noarch                         18/61
  Installing       : perl-Pod-Perldoc-3.28-396.el8.noarch                     19/61
  Installing       : perl-Text-ParseWords-3.30-395.el8.noarch                 20/61
  Installing       : perl-Pod-Usage-4:1.69-395.el8.noarch                     21/61
  Installing       : perl-MIME-Base64-3.15-396.el8.x86_64                     22/61
  Installing       : perl-Storable-1:3.11-3.el8.x86_64                        23/61
  Installing       : perl-Getopt-Long-1:2.50-4.el8.noarch                     24/61
  Installing       : perl-Errno-1.28-420.el8.x86_64                           25/61
  Installing       : perl-Socket-4:2.027-3.el8.x86_64                         26/61
  Installing       : perl-Encode-4:2.97-3.el8.x86_64                          27/61
  Installing       : perl-Carp-1.42-396.el8.noarch                            28/61
  Installing       : perl-Exporter-5.72-396.el8.noarch                        29/61
  Installing       : perl-libs-4:5.26.3-420.el8.x86_64                        30/61
  Installing       : perl-Scalar-List-Utils-3:1.49-2.el8.x86_64               31/61
  Installing       : perl-parent-1:0.237-1.el8.noarch                         32/61
  Installing       : perl-macros-4:5.26.3-420.el8.x86_64                      33/61
  Installing       : perl-Text-Tabs+Wrap-2013.0523-395.el8.noarch             34/61
  Installing       : perl-Unicode-Normalize-1.25-396.el8.x86_64               35/61
  Installing       : perl-File-Path-2.15-2.el8.noarch                         36/61
  Installing       : perl-IO-1.38-420.el8.x86_64                              37/61
  Installing       : perl-PathTools-3.74-1.el8.x86_64                         38/61
  Installing       : perl-constant-1.33-396.el8.noarch                        39/61
  Installing       : perl-threads-1:2.21-2.el8.x86_64                         40/61
  Installing       : perl-threads-shared-1.58-2.el8.x86_64                    41/61
  Installing       : perl-interpreter-4:5.26.3-420.el8.x86_64                 42/61
  Installing       : libxslt-1.1.32-6.el8.x86_64                              43/61
  Installing       : fontconfig-2.13.1-4.el8.x86_64                           44/61
  Running scriptlet: fontconfig-2.13.1-4.el8.x86_64                           44/61
  Running scriptlet: nginx-filesystem-1:1.14.1-9.module+el8.4.0+542+8154722   45/61
  Installing       : nginx-filesystem-1:1.14.1-9.module+el8.4.0+542+8154722   45/61
  Installing       : libwebp-1.0.0-5.el8.x86_64                               46/61
  Installing       : libXau-1.0.9-3.el8.x86_64                                47/61
  Installing       : libxcb-1.13.1-1.el8.x86_64                               48/61
  Installing       : libX11-common-1.6.8-5.el8.noarch                         49/61
  Installing       : libX11-1.6.8-5.el8.x86_64                                50/61
  Installing       : libXpm-3.5.12-8.el8.x86_64                               51/61
  Installing       : jbigkit-libs-2.1-14.el8.x86_64                           52/61
  Running scriptlet: jbigkit-libs-2.1-14.el8.x86_64                           52/61
  Installing       : libtiff-4.0.9-20.el8.x86_64                              53/61
  Installing       : gd-2.2.5-7.el8.x86_64                                    54/61
  Running scriptlet: gd-2.2.5-7.el8.x86_64                                    54/61
  Installing       : nginx-mod-http-perl-1:1.14.1-9.module+el8.4.0+542+8154   55/61
  Running scriptlet: nginx-mod-http-perl-1:1.14.1-9.module+el8.4.0+542+8154   55/61
  Installing       : nginx-mod-http-xslt-filter-1:1.14.1-9.module+el8.4.0+5   56/61
  Running scriptlet: nginx-mod-http-xslt-filter-1:1.14.1-9.module+el8.4.0+5   56/61
  Installing       : nginx-mod-mail-1:1.14.1-9.module+el8.4.0+542+81547229.   57/61
  Running scriptlet: nginx-mod-mail-1:1.14.1-9.module+el8.4.0+542+81547229.   57/61
  Installing       : nginx-mod-stream-1:1.14.1-9.module+el8.4.0+542+8154722   58/61
  Running scriptlet: nginx-mod-stream-1:1.14.1-9.module+el8.4.0+542+8154722   58/61
  Installing       : nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64      59/61
  Running scriptlet: nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64      59/61
  Installing       : nginx-mod-http-image-filter-1:1.14.1-9.module+el8.4.0+   60/61
  Running scriptlet: nginx-mod-http-image-filter-1:1.14.1-9.module+el8.4.0+   60/61
  Installing       : nginx-all-modules-1:1.14.1-9.module+el8.4.0+542+815472   61/61
  Running scriptlet: nginx-all-modules-1:1.14.1-9.module+el8.4.0+542+815472   61/61
  Running scriptlet: fontconfig-2.13.1-4.el8.x86_64                           61/61
  Verifying        : gd-2.2.5-7.el8.x86_64                                     1/61
  Verifying        : jbigkit-libs-2.1-14.el8.x86_64                            2/61
  Verifying        : libX11-1.6.8-5.el8.x86_64                                 3/61
  Verifying        : libX11-common-1.6.8-5.el8.noarch                          4/61
  Verifying        : libXau-1.0.9-3.el8.x86_64                                 5/61
  Verifying        : libXpm-3.5.12-8.el8.x86_64                                6/61
  Verifying        : libjpeg-turbo-1.5.3-12.el8.x86_64                         7/61
  Verifying        : libtiff-4.0.9-20.el8.x86_64                               8/61
  Verifying        : libwebp-1.0.0-5.el8.x86_64                                9/61
  Verifying        : libxcb-1.13.1-1.el8.x86_64                               10/61
  Verifying        : nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64      11/61
  Verifying        : nginx-all-modules-1:1.14.1-9.module+el8.4.0+542+815472   12/61
  Verifying        : nginx-filesystem-1:1.14.1-9.module+el8.4.0+542+8154722   13/61
  Verifying        : nginx-mod-http-image-filter-1:1.14.1-9.module+el8.4.0+   14/61
  Verifying        : nginx-mod-http-perl-1:1.14.1-9.module+el8.4.0+542+8154   15/61
  Verifying        : nginx-mod-http-xslt-filter-1:1.14.1-9.module+el8.4.0+5   16/61
  Verifying        : nginx-mod-mail-1:1.14.1-9.module+el8.4.0+542+81547229.   17/61
  Verifying        : nginx-mod-stream-1:1.14.1-9.module+el8.4.0+542+8154722   18/61
  Verifying        : perl-Digest-1.17-395.el8.noarch                          19/61
  Verifying        : perl-Digest-MD5-2.55-396.el8.x86_64                      20/61
  Verifying        : perl-IO-Socket-IP-0.39-5.el8.noarch                      21/61
  Verifying        : perl-IO-Socket-SSL-2.066-4.module+el8.4.0+512+d4f0fc54   22/61
  Verifying        : perl-Mozilla-CA-20160104-7.module+el8.4.0+529+e3b3e624   23/61
  Verifying        : perl-Net-SSLeay-1.88-1.module+el8.4.0+512+d4f0fc54.x86   24/61
  Verifying        : perl-URI-1.73-3.el8.noarch                               25/61
  Verifying        : perl-libnet-3.11-3.el8.noarch                            26/61
  Verifying        : fontconfig-2.13.1-4.el8.x86_64                           27/61
  Verifying        : libxslt-1.1.32-6.el8.x86_64                              28/61
  Verifying        : perl-Carp-1.42-396.el8.noarch                            29/61
  Verifying        : perl-Data-Dumper-2.167-399.el8.x86_64                    30/61
  Verifying        : perl-Encode-4:2.97-3.el8.x86_64                          31/61
  Verifying        : perl-Errno-1.28-420.el8.x86_64                           32/61
  Verifying        : perl-Exporter-5.72-396.el8.noarch                        33/61
  Verifying        : perl-File-Path-2.15-2.el8.noarch                         34/61
  Verifying        : perl-File-Temp-0.230.600-1.el8.noarch                    35/61
  Verifying        : perl-Getopt-Long-1:2.50-4.el8.noarch                     36/61
  Verifying        : perl-HTTP-Tiny-0.074-1.el8.noarch                        37/61
  Verifying        : perl-IO-1.38-420.el8.x86_64                              38/61
  Verifying        : perl-MIME-Base64-3.15-396.el8.x86_64                     39/61
  Verifying        : perl-PathTools-3.74-1.el8.x86_64                         40/61
  Verifying        : perl-Pod-Escapes-1:1.07-395.el8.noarch                   41/61
  Verifying        : perl-Pod-Perldoc-3.28-396.el8.noarch                     42/61
  Verifying        : perl-Pod-Simple-1:3.35-395.el8.noarch                    43/61
  Verifying        : perl-Pod-Usage-4:1.69-395.el8.noarch                     44/61
  Verifying        : perl-Scalar-List-Utils-3:1.49-2.el8.x86_64               45/61
  Verifying        : perl-Socket-4:2.027-3.el8.x86_64                         46/61
  Verifying        : perl-Storable-1:3.11-3.el8.x86_64                        47/61
  Verifying        : perl-Term-ANSIColor-4.06-396.el8.noarch                  48/61
  Verifying        : perl-Term-Cap-1.17-395.el8.noarch                        49/61
  Verifying        : perl-Text-ParseWords-3.30-395.el8.noarch                 50/61
  Verifying        : perl-Text-Tabs+Wrap-2013.0523-395.el8.noarch             51/61
  Verifying        : perl-Time-Local-1:1.280-1.el8.noarch                     52/61
  Verifying        : perl-Unicode-Normalize-1.25-396.el8.x86_64               53/61
  Verifying        : perl-constant-1.33-396.el8.noarch                        54/61
  Verifying        : perl-interpreter-4:5.26.3-420.el8.x86_64                 55/61
  Verifying        : perl-libs-4:5.26.3-420.el8.x86_64                        56/61
  Verifying        : perl-macros-4:5.26.3-420.el8.x86_64                      57/61
  Verifying        : perl-parent-1:0.237-1.el8.noarch                         58/61
  Verifying        : perl-podlators-4.11-1.el8.noarch                         59/61
  Verifying        : perl-threads-1:2.21-2.el8.x86_64                         60/61
  Verifying        : perl-threads-shared-1.58-2.el8.x86_64                    61/61

Installed:
  fontconfig-2.13.1-4.el8.x86_64
  gd-2.2.5-7.el8.x86_64
  jbigkit-libs-2.1-14.el8.x86_64
  libX11-1.6.8-5.el8.x86_64
  libX11-common-1.6.8-5.el8.noarch
  libXau-1.0.9-3.el8.x86_64
  libXpm-3.5.12-8.el8.x86_64
  libjpeg-turbo-1.5.3-12.el8.x86_64
  libtiff-4.0.9-20.el8.x86_64
  libwebp-1.0.0-5.el8.x86_64
  libxcb-1.13.1-1.el8.x86_64
  libxslt-1.1.32-6.el8.x86_64
  nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
  nginx-all-modules-1:1.14.1-9.module+el8.4.0+542+81547229.noarch
  nginx-filesystem-1:1.14.1-9.module+el8.4.0+542+81547229.noarch
  nginx-mod-http-image-filter-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
  nginx-mod-http-perl-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
  nginx-mod-http-xslt-filter-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
  nginx-mod-mail-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
  nginx-mod-stream-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64
  perl-Carp-1.42-396.el8.noarch
  perl-Data-Dumper-2.167-399.el8.x86_64
  perl-Digest-1.17-395.el8.noarch
  perl-Digest-MD5-2.55-396.el8.x86_64
  perl-Encode-4:2.97-3.el8.x86_64
  perl-Errno-1.28-420.el8.x86_64
  perl-Exporter-5.72-396.el8.noarch
  perl-File-Path-2.15-2.el8.noarch
  perl-File-Temp-0.230.600-1.el8.noarch
  perl-Getopt-Long-1:2.50-4.el8.noarch
  perl-HTTP-Tiny-0.074-1.el8.noarch
  perl-IO-1.38-420.el8.x86_64
  perl-IO-Socket-IP-0.39-5.el8.noarch
  perl-IO-Socket-SSL-2.066-4.module+el8.4.0+512+d4f0fc54.noarch
  perl-MIME-Base64-3.15-396.el8.x86_64
  perl-Mozilla-CA-20160104-7.module+el8.4.0+529+e3b3e624.noarch
  perl-Net-SSLeay-1.88-1.module+el8.4.0+512+d4f0fc54.x86_64
  perl-PathTools-3.74-1.el8.x86_64
  perl-Pod-Escapes-1:1.07-395.el8.noarch
  perl-Pod-Perldoc-3.28-396.el8.noarch
  perl-Pod-Simple-1:3.35-395.el8.noarch
  perl-Pod-Usage-4:1.69-395.el8.noarch
  perl-Scalar-List-Utils-3:1.49-2.el8.x86_64
  perl-Socket-4:2.027-3.el8.x86_64
  perl-Storable-1:3.11-3.el8.x86_64
  perl-Term-ANSIColor-4.06-396.el8.noarch
  perl-Term-Cap-1.17-395.el8.noarch
  perl-Text-ParseWords-3.30-395.el8.noarch
  perl-Text-Tabs+Wrap-2013.0523-395.el8.noarch
  perl-Time-Local-1:1.280-1.el8.noarch
  perl-URI-1.73-3.el8.noarch
  perl-Unicode-Normalize-1.25-396.el8.x86_64
  perl-constant-1.33-396.el8.noarch
  perl-interpreter-4:5.26.3-420.el8.x86_64
  perl-libnet-3.11-3.el8.noarch
  perl-libs-4:5.26.3-420.el8.x86_64
  perl-macros-4:5.26.3-420.el8.x86_64
  perl-parent-1:0.237-1.el8.noarch
  perl-podlators-4.11-1.el8.noarch
  perl-threads-1:2.21-2.el8.x86_64
  perl-threads-shared-1.58-2.el8.x86_64

Complete!
```
### Lancer le service nginx :
```bash
[christopher@proxy ~]$ systemctl start nginx.service
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'nginx.service'.
Multiple identities can be used for authentication:
 1.  christopher
 2.  test
Choose identity to authenticate as (1-2): 1
Password:
==== AUTHENTICATION COMPLETE ====
```
### On check qu'il est actif :
```bash
[christopher@proxy ~]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: >
   Active: active (running) since Mon 2021-12-13 13:58:46 CET; 1min 16s ago
  Process: 9395 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 9393 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 9391 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/S>
 Main PID: 9396 (nginx)
    Tasks: 5 (limit: 23520)
   Memory: 7.6M
   CGroup: /system.slice/nginx.service
           ├─9396 nginx: master process /usr/sbin/nginx
           ├─9397 nginx: worker process
           ├─9398 nginx: worker process
           ├─9399 nginx: worker process
           └─9400 nginx: worker process

Dec 13 13:58:46 node1.tp1.cesi systemd[1]: Starting The nginx HTTP and reverse prox>
Dec 13 13:58:46 node1.tp1.cesi nginx[9393]: nginx: the configuration file /etc/ngin>
Dec 13 13:58:46 node1.tp1.cesi nginx[9393]: nginx: configuration file /etc/nginx/ng>
Dec 13 13:58:46 node1.tp1.cesi systemd[1]: Started The nginx HTTP and reverse proxy>
lines 1-20/20 (END)
```
### Ouvrir le port dans le pare-feu :
```bash
[christopher@proxy ~]$ sudo firewall-cmd --permanent --add-port=80/tcp
success
[christopher@proxy ~]$ sudo firewall-cmd --reload
success
```
### Test du bon fonctionnement de NGINX depuis mon poste local : 
```bash
PS C:\Windows\system32> curl 192.168.181.139:80


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

                    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
                      <head>
                        <title>Test Page for the Nginx...
RawContent        : HTTP/1.1 200 OK
                    Connection: keep-alive
                    Accept-Ranges: bytes
                    Content-Length: 3429
                    Content-Type: text/html
                    Date: Mon, 13 Dec 2021 13:04:24 GMT
                    ETag: "60c1d6af-d65"
                    Last-Modified: Thu, 10 Jun 2021...
Forms             : {}
Headers           : {[Connection, keep-alive], [Accept-Ranges, bytes], [Content-Length, 3429], [Content-Type, text/html]...}
Images            : {@{innerHTML=; innerText=; outerHTML=<IMG alt="[ Powered by nginx ]" src="nginx-logo.png" width=121 height=32>;
                    outerText=; tagName=IMG; alt=[ Powered by nginx ]; src=nginx-logo.png; width=121; height=32}, @{innerHTML=;
                    innerText=; outerHTML=<IMG alt="[ Powered by Rocky Linux ]" src="poweredby.png" width=88 height=31>; outerText=;
                    tagName=IMG; alt=[ Powered by Rocky Linux ]; src=poweredby.png; width=88; height=31}}
InputFields       : {}
Links             : {@{innerHTML=Rocky Linux website; innerText=Rocky Linux website; outerHTML=<A
                    href="https://www.rockylinux.org/">Rocky Linux website</A>; outerText=Rocky Linux website; tagName=A;
                    href=https://www.rockylinux.org/}, @{innerHTML=available on the Rocky Linux website; innerText=available on the
                    Rocky Linux website; outerHTML=<A href="https://www.rockylinux.org/">available on the Rocky Linux website</A>;
                    outerText=available on the Rocky Linux website; tagName=A; href=https://www.rockylinux.org/}, @{innerHTML=<IMG
                    alt="[ Powered by nginx ]" src="nginx-logo.png" width=121 height=32>; innerText=; outerHTML=<A
                    href="http://nginx.net/"><IMG alt="[ Powered by nginx ]" src="nginx-logo.png" width=121 height=32></A>;
                    outerText=; tagName=A; href=http://nginx.net/}, @{innerHTML=<IMG alt="[ Powered by Rocky Linux ]"
                    src="poweredby.png" width=88 height=31>; innerText=; outerHTML=<A href="http://www.rockylinux.org/"><IMG alt="[
                    Powered by Rocky Linux ]" src="poweredby.png" width=88 height=31></A>; outerText=; tagName=A;
                    href=http://www.rockylinux.org/}}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 3429
```
 ## Configurer NGINX comme reverse proxy + HTTPS + Forcer vers HTTPS
 Créer le certificat pour le HTTPS
 ```bash
 sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/pki/tls/private/send.key -out /etc/pki/tls/certs/send.crt
 ```
 On éclate la conf et on sort le module qui gère le reverse proxy dans un autre fichier "send.conf"
```bash
[christopher@proxy ~]$ cat /etc/nginx/nginx.conf
user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;
    include /etc/nginx/conf.d/*.conf;
}


```

## Conf Send
```bash
[christopher@proxy ~]$ cat /etc/nginx/conf.d/send.conf
    server {
        listen       80;
        listen       [::]:80;
        server_name  send.wood.com;

    location / {
        return 301 https://$host$request_uri;
        }
    }

    server {
        listen 443 ssl http2;
        server_name send.wood.com;
        ssl_certificate /etc/pki/tls/certs/send.crt;
        ssl_certificate_key /etc/pki/tls/private/send.key;

        location / {
              proxy_pass http://send.wood.com;
              proxy_set_header X-Real-IP         $remote_addr;
              proxy_set_header X-Forwarded-Proto $scheme;
              proxy_set_header X-Forwarded-Host  $host;
              proxy_set_header X-Forwarded-Port  $server_port;
                }
            }
```
 Comme j'ai mis `proxy_pass http://send.wood.com;`, j'ai aussi modifié le hosts

```bash
[christopher@proxy ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.181.134    send.wood.com
```
Test depuis ma machine locale, il faut ajouter le nom dans notre fichier hosts aussi

[En image](https://media.discordapp.net/attachments/576780826554007584/934030457744551987/Pasted_image_20220116165201.png)

# BORG Backup
## Installer BORG
```bash
[christopher@backup ~]$ curl -SLO https://github.com/borgbackup/borg/releases/download/1.1.17/borg-linux64
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   649  100   649    0     0   1996      0 --:--:-- --:--:-- --:--:--  1996
100 18.0M  100 18.0M    0     0  13.5M      0  0:00:01  0:00:01 --:--:-- 30.7M
[christopher@backup ~]$ sudo cp borg-linux64 /usr/local/bin/borg
[sudo] password for christopher:
[christopher@backup ~]$ sudo chown root:root /usr/local/bin/borg
[christopher@backup ~]$ sudo chmod 755 /usr/local/bin/borg
```
## Créer un repo
On créera un répo par serveur, ici on fait le repo pour le proxy Nginx
```bash
[christopher@Backup backups-nginx]$ sudo mkdir /backup
[christopher@Backup backups-nginx]$ sudo mkdir /backup/proxy-nginx
[christopher@Backup backups-nginx]$ sudo /usr/local/bin/borg init --encryption=repokey /backup/proxy-nginx
Enter new passphrase:
Enter same passphrase again:
Do you want your passphrase to be displayed for verification? [yN]: n

By default repositories initialized with this version will produce security
errors if written to with an older version (up to and including Borg 1.0.8).

If you want to use these older versions, you can disable the check by running:
borg upgrade --disable-tam /backup/proxy-nginx

See https://borgbackup.readthedocs.io/en/stable/changes.html#pre-1-0-9-manifest-spoofing-vulnerability for details about the security implications.

IMPORTANT: you will need both KEY AND PASSPHRASE to access this repo!
If you used a repokey mode, the key is stored in the repo, but you should back it up separately.
Use "borg key export" to export the key, optionally in printable format.
Write down the passphrase. Store both at safe place(s).
```
## Automatiser BORG
### Le script automatique
On veut ici sauvegarder le proxy nginx sur le serveur 192.168.181.139
Le nom de chaque backup sera nginx_AnnéeMoisJour_HeureMinuteSeconde
```bash
export BORG_PASSPHRASE=passphrase
var=$(printf 'nginx_%(%Y%m%d_%H%M%S)T')
borg create --compression lzma,9 /backup/proxy-nginx::$var root@192.168.181.139:/etc/nginx/
```
Dans notre cas on met le mot de passe du repo dans le script, il faudra alors reserrer les permissions pour être le seul à pouvoir voir/modifier le script
[christopher@backup ~]$ sudo chown christopher:christopher /etc/scripts.borg_save_nginx.sh
[christopher@backup ~]$ sudo chmod 700 /etc/scripts.borg_save_nginx.sh
### Le service
On crée un service qui exécutera notre script à chaque fois qu'il sera lancé
```bash
[christopher@backup ~]$ sudo cat /etc/systemd/system/backup_db.service
[Unit]
Description=<DESCRIPTION>

[Service]
ExecStart=bash /etc/scripts/borg_save_nginx.sh
Type=oneshot

[Install]
WantedBy=multi-user.target
```
### Le timer du service
On crée un timer afin d'exécuter le service qui exécutera notre script.
Ici le timer lancera le backup toutes les heures arrondi à l'heure près et au démarrage du serveur
```bash
[christopher@backup ~]$ sudo cat /etc/systemd/system/backup.timer
[Unit]
Description=Lance backup.service à intervalles réguliers
Requires=backup_db.service

[Timer]
Unit=backup_db.service
OnCalendar=hourly

[Install]
WantedBy=timers.target
```
### Checker que le timer est effectif
```bash
[christopher@backup ~]$ sudo systemctl daemon-reload
[christopher@backup ~]$ sudo systemctl start backup.timer
[christopher@backup ~]$ sudo systemctl enable backup.timer
Created symlink /etc/systemd/system/timers.target.wants/backup.timer → /etc/systemd/                                                                                                                          system/backup.timer.
[christopher@backup ~]$ sudo systemctl list-timers
NEXT                         LEFT       LAST                         PASSED       U>
Wed 2021-12-15 15:00:00 CET  39min left n/a                          n/a          b>
Wed 2021-12-15 15:09:23 CET  49min left Wed 2021-12-15 13:53:25 CET  26min ago    d>
Thu 2021-12-16 00:00:00 CET  9h left    Wed 2021-12-15 03:40:00 CET  10h ago      m>
Thu 2021-12-16 08:49:49 CET  18h left   Wed 2021-12-15 08:49:49 CET  5h 30min ago s>

4 timers listed.
```


https://borgbackup.readthedocs.io/en/stable/

https://gitlab.com/timvisee/send

https://gitlab.com/it4lik/cesi2-linux-2021/-/tree/main

https://docs.docker.com/engine/install/centos/

https://gitlab.com/Elodega/linuxrendutp
