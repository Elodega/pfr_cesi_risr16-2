# Projet fil rouge

```toc
    style: number
    min_depth: 1
    max_depth: 6
```

Compteur de serveur :
- web  : 9
- db  : 5

```ad-note
title: BLABLA*
Pas dans le sujet mais conseillé
```
## Infra :
```ad-note
Mettre 60% du budget dedans n'est pas choquant
```
```ad-tip
title: ESXi
Il y a des clefs fournie par le CESI
```
### Virtualisation (compute)
- ESXi | vSphere (web) | vCenter (web)
	- Si le budget suit c'est le meilleur choix
- Hyper-V
	- Est OK, pas trop cher, s'intègre avec les autres Windows
- Qemu/KVM | Proxmox
	- Qemu permet de faire des configurations très fines et d'avoir des VM opti
	- Mais faut avoir un savoir énorme
	- À ne pas prendre pour le PFR


```ad-tip
title: Appliance
Application livrée prête à l'emploie
```

### Stockage
- Baie de stockage (+ appliance)
	- Dell/EMC
	- NetApp
	- Huawei
		- Moins cher
		- Support moins bien
	- Technologie de stockage
		- **DFS** (Distributed File System)
			- Permet d'aboir une seule arborescence de disque qui peut s'étendre sur plusieurs disques de plusieurs machines
			- On lui demande 20 Gb, il les fou n'importe où (voir sur différent disque) et pour nous c'est transparent
			- Techno Windows > Serveur only windows mais client osef
		- Ceph
			- Même que DFS mais pour Linux
			- C'est plus compliqué
			- Coûte plus cher au lancement (car faut les connaissances)
			- Customisable
		- Gluster
			- Est fait pour faire de la réplication longue distance ( plusieurs secondes de latence)

```ad-warning
title: Mise à jour
Dans la partie sécu, la gestion des MAJ (autant des OS Server que de l'infra) doit être au top
```

### OS Server
- Windows Server
- Linux
	- Pas d'hétérogénéité 
- BSD
	- Routeur/Switch Cisco
	- Demande des connaissances très poussées
### Deployment
- VM
	- Terraform
	- vRealize | vRO
- OS + conf (réseau, maj, domaine)
	- Redhat
		- KickStart
	- Windows Server
		- Ca existe mais j'ai pas le nom
- Application
	- Ansible
	- Puppet 
	- Windows Deployment Service
## Base :
### DNS
- Windows
- Bind9
	- Tous les DNS mondiaux tournent sous Bind
	- Faut écire les fichiers de zone à la main
- PowerDNS
	- Plus facile d'utilisation
- IPAM (Ip Address Management)
	- Inventaire exhaustif des machines sur le parc (Vérité vraie via le DNS)
	- Netbox
		- Gestion de datacenter couplé à la gestion DNS
		- Coûteux à mettre en place
		- Grosse plus value
### DHCP
- Windows
- Linux
	- ISC DHCP
### NTP
- Windows
- Linux
	- NTPD
	- Chrony
### LDAP
- Windows
- Linux
	- OpenLDAP
		- LDAPphpmyadmin
	- FreeIPA

```ad-tip
title: Monitoring
Envoie des alertes quand ca dépasse un seuil
```
### Monitoring (Web)
- Zabbix
	- Bon choix d'après le sujet
- Centreon (Coeur Nagios)
- Nagios
	- C'est le plus vieux
- Prometheus
	- C'est plus une BDD
- Netdata
	- Plus moderne
	- Ultra performant
	- Interface graphique
	- Alerting
	- Cas réel, on utilise Netdata pour récupérer les données et on les envoie à un autre outil
- Pour tester un outil de monitoring
	- Doit monitorer :
		- Tout
		- Ouverture de port
		- Remplissage de disque
	- Multi-OS
```ad-tip
title: Métrologie
Trace des courbes pour prévoir qu'un truc va tomber
```
### Métrologie
- Netdata
- Prometheus + Graphana/Kibana
### Ticketing (Web)
- GLPI
- Jira
- Service Now
- IWS
- ITOP
	- Fait aussi inventaire etc comme GLPI (CMDB)
```ad-tip
title: Déduplication file system
Les fichiers sont stockés par bloc (exemple de 4096 octets), un outil de backup qui gère la déduplication il ne copiera pas plusieurs fois un même bloc d'octet.
Si on backup 2 fois un même dossier il a peu changé, le backup sera très petit
```
### Backup
```ad-tip
title: Securité
Disponibilité, intégrité, confidentialité
Le backup rentre dans ces cases
```

- Logiciel
	- Veeam
		- Veeam backup & restore community edition
			- Jusqu'à 1 ESXi avec 10 machines dedans
	- Borg
		- Open-Source
		- Simple d'utilisation
		- Un des plus performant au monde
		- Déduplication file system
		- Script via un service qui crée le backup et l'exporte dans un dossier partagé
	- Acronis
	- Avamar
	- rsync
- Espace de stockage dédié
	- Baie de stockage dédiée
	- Disque dur éclaté, on a jamais besoin de lire normalement
- Durée de rétention
	- Cas par cas
		- Criticité
			- 0 à 5
		- Combien de temps on veut garder
			- En fonction de la criticité
			- Fonction de l'app
			- Quantité
		- Qu'est-ce qu'on backup ? Concrètement et exhaustivement 
			- Ca doit permettre de restaurer l'infra si on perd tout
```ad-tip
title: Reverse Proxy
Augmente les performances des serveurs individuels on les débarrassant de certaines tâches.
Augmente la sécurité en restreignant les points d'entrées
```


### Reverse Proxy*
- Apache
- IIS
- Nginx	
	- Référence

```ad-tip
title: Redondance de Reverse Proxy
On peut fonctionner via une Virtual IP mais c'est obsolète
Maintenant on installe le load balancing via le DNS
On met 2 entrées DNS, une pour chaque server Reverse Proxy. Elles auront le même nom mais pointeront chacune sur un serveur
```

## Others
### Mail 
- Exchange
	- Client mail webui
- Postfix x Devocot
- Bluemind
	- Client mail webui
- Zimbra
	- Client mail webui
- Antispam
	- Spamassassin
	- Sophos
	- Proofpoint
	- Proxmox Mail Gateway
### ERP (bdd)
- Odoo
- MB
- ERPNEXT
- Microsoft Dynamics
- Dolibarr
### Espace documentaire (bdd + web)*
- Wiki
	- Wikijs
		- Markdown
		- Gestion de droit
	- Mediawiki
		- Gestion de droit
- Confluence
- Sharepoint
### Site vitrine (bdd+web)*
### File share
- Nextcloud
- Sharepoint
- Onedrive
- Google Drive
- Dropbox
	- À bannir
- Wetransfer
	- À bannir
- Samba
	- Intégré à l'OS
- [Send](https://gitlab.com/timvisee/send)
	- Chiffré de bout en bout
	- Performant
	- Durée de rétention
		- Le fichier reste x temps
			- Le fichier reste 1 semaine
			- Le fichier reste 1 jour
			- Le fichier est a usage unique
	- Ecologique
		- Performant
		- Durée de rétention
	- [Instance publique](https://send.vis.ee/)

### Print Server
### Proxy Client
- Squid
- Pi-hole
- Forti
- Sophos

### VPN Client
- Wireguard
	- Facile à setup et conf
	- Ultra performant
	- Utilise les derniers algo de chiffrement
- Sophos
- Forti
- Pfsense
- Open VPN
	- Mature
	- Chiant à setup et maintenir
- WatchGuard
### Logs*
- Rsyslog
- Splunk
- Elastic
	- Elasticsearch, Logstach et Kibana
### CA/PublicKeyInfrastructure*
- Windows

```ad-warning
title: CMDB
À intégrer dans un logiciel déjà existant style GLPI
Elle doit obligatoirement être 100% automatisée
```

## Cloud ou pas ?
- Virtu et stockage 
	- À garder en interne
	- Il faut garder une infra en interne pour faire tourner un minimum de service
- LDAP
	- Donnée sensible
- Monitoring
	- Donnée sensible
- Métrologie
	- Donnée sensible
- Ticketing
	- Donnée un peu sensible
- Backup
	- Très sensible
- Reverse Proxy
	- CloudFlare
- ERP
	- Très sensible
- Espace Documentaire
	- Très sensible
- File Share
	- Ca doit être rapide on n'a pas besoin qu'il fasse un aller retour avec l'extérieur
- Logs
	- Très très critique
- CA/PKI
	- Interne obligatoire
- CMDB
	- Sensible

```ad-tip
Faire une étude de criticitée (score)
```
Offre de cloud :
- OVH
	- RGPD
	- Mature
	- Transparent
- AWS
- Azure
	- Fais pour les parcs only Windows
- IBM Cloud
- Cheops
## Libre/Foss
### Libre
- Licence
	- GLP+3
	- MiT
	- BSD
		- Licence libre qui permet le secret industriel
		- Il n'a pas la contrainte de faire perdurer la licence libre
- Open Source
	- Accès au code
- Libre
	- Garantir vos libertés
		- Savoir 
			- Open Source
		- Modification
		- Redistribué
	- On doit faire perdurer la licence libre
### FreeOpenSources
- Modulable/Customisable
- Transparence
	- Sécurité
- Pédagogie / Partage de connaissance
- Communauté
	- Retour d'expérience
	- Documentation
	- Confiance
- Perspectives différentes
	- Contribue à créer un projet unique
	- Qualité
## Bonnes pratiques / Standards


# Préparer l'entretien
## Contexte
Je représente une petite entreprise de 350 personnes qui devrait assez vite atteindre les 450 personnes, le cloud nous permettrait normalement de gérer l'évolution de l'infrastructure et c'est pour ça qu'on qu'aujourd'hui je m'intéresse aux différentes offres de cloud provider et d'infogérance. Le but de l'entretien c'est surtout d'avoir un panorama des différentes offres d'hébergement en France. Nous serions en premier lieu intéressé par votre offre de colocation dans un datacenter en France, l'entreprise est assez tournée vers l'écologie alors je souhaiterais aussi bien avoir les informations pour vos Datacenter que pour vos Datacenter neutre. De plus pour l'heure chacun de nos sites a un FAI différents j'aurais aussi quelques questions vis-à-vis de votre offre internet

## Infra
L'infrastructure serait hybride avec tout ce qu'on nous catégorisons comme sensible en interne et tout le reste chez le cloud provider que nous aurons choisis. 
J'aimerais être sûr de n'avoir rien loupé sur votre site, vous n'intervenez pas sur la parte OS du tout, vous ne faites pas de monitoring, vous ne vous occupez que de la partie physique des serveurs
Il y a 14 serveurs ce qui représente 20 vCPU et 90 vRAM pour une capacité de stockage de 6 To. 

Internet
Vous devez raccorder à votre réseau chaque nouveau client ?
Il y a un site à Lille, c'est le siège social du groupe, un site à Dax, un à Annecy ainsi que 2 autres à Macon et Brest. J'ai pu voir que vous étiez présent à Lille mais je n'ai pas vu Dax, est-ce que c'est possible d'être raccordé à votre réseau ?

 ### Détail de la conso
 
#### DNS + AD : 
- Windows 2CPU / 4Go RAM / 40 Go Stockage HDD

#### DHCP : 
- Windows 2CPU / 4Go RAM / 40 Go Stockage HDD

#### NTP : 
- Windows 2CPU / 4Go RAM / 40 Go Stockage HDD

#### WSUS

#### Antivirus

#### Monitoring (Web) : 
- Linux 2CPU / 2Go RAM / 150 Go Stockage SSD
	- Zabbix askip c'est bien mais je ne connais pas
	- Nagios c'est chiant à monter et c'est vieillot
	- Centreons

#### Métrologie : 
- Est-ce qu'on fait ?
- Linux 2CPU / 2Go RAM / 150 Go Stockage SSD
	- Netdata
	- Prometheus + Graphana/Kibana

#### Ticketing (Web) : 
- Linux 2CPU / 2Go RAM / 20 Go Stockage HDD
	- GLPI
		- Facile à monter
		- Facile à administrer
		- Peut intégrer une CMDB 
		- Peut faire l'inventaire avec fusion

#### Backup : 
- Linux 2CPU / 2Go RAM / 4 To Stockage HDD Si interne sinon ca évoluera avec le temps base 500 Go HDD
	- Borg
- Windows 2CPU / 4Go RAM / 4 To Stockage HDD Si interne sinon ca évoluera avec le temps base 500 Go HDD
	- Infogérant
	- Veeam

#### Reverse Proxy* : 
- Est-ce qu'on fait ?
- Linux 2CPU / 2Go RAM / 20 Go Stockage HDD
	- Nginx 
		- C'est fait pour ça
		- Très sécure
		- On sait faire

#### Mail : 
- Exchange Pas d'idée de spec
- Les solutions sous Linux sont chiantes à monter

#### ERP (interne) :
- Microsoft dynamics 4CPU / 8Go RAM / 80 Go Stockage SSD

#### Espace documentaire (bdd + web)* ( Possiblement interne) :
- Est-ce qu'on fait ?
- Linux 2CPU / 2Go RAM / 40 Go Stockage HDD
	- Mediawiki
		- Pas WikiJS car je ne pense pas que beaucoup dans leur boîte utilise le md

#### Site vitrine (bdd+web)* :
- Est-ce qu'on fait ? Jusqu'où on va dans la maquette si on fait
- Linux
	- Nginx ? 
	- Wordpress (jms fait)

#### File share (Possiblement interne) : 
- Linux 2CPU / 2Go RAM / 100 Go Stockage SSD ou 200 Go HDD
	- Send en interne
	
#### Proxy Client : 
- Squid
- Pihole (Filtrage par DNS)
- Le firewall qu'on prend

#### VPN Client : 
 - Le firewall qu'on prend
 - Wireguard
	- Facile à setup et conf
	- Ultra performant
	- Utilise les derniers algo de chiffrement

#### Logs* : 
- Rsyslog 2CPU / 2Go RAM / 100 Go HDD
	- Utilise le truc natif aux os SYSLOG
	- Jsp s'il y a une interface avec
		- Il faut surement installer Kibana ou Graphana dessus
- Splunk
	- Fonctionne du tonnerre
	- Grosse commu
	- Très puissant
	- Payant (1800e/an/Gb de log/jour)

#### CA/PublicKeyInfrastructure* : 
- Est-ce qu'on fait ?
- Windows

#### Serveur de fichier (interne) :
- Linux 2CPU / 2Go RAM / 1 To HDD (hasard)
	- Gitlab

"Un niveau de sécurité accentué, mais aussi la mise en place d’une solution de traçabilité des accès devra être incluse dans la proposition technique."
> Norme HDS chez CIS, ca enregistre les accès et filme les actions

Un niveau de sécurité accentué, mais aussi la mise en place d’une solution de traçabilité des accès devra être incluse dans la proposition technique.

Une part importante à la mobilité devra être intégrée au projet, un collaborateur devra être connecté sur le même réseau qu’il soit à son bureau ou en salle de réunion, son ordinateur devra être reconnu sur n’importe quel site de l’entreprise et avoir des accès aux ressources (fichiers, fichiers R&D…) de façon sécurisée depuis n’importe quel site de l’entreprise.

### Caractéristiques envisagées :
#### En interne
 - CPU : 6 (+ 4 possible)
 - RAM (en Go) : 10 (+ 4 possible)
 - HDD (en Go) : 1000 (+ 4240 possible)
 - SSD (en Go) : 80 (+ 100 possible)

#### En externe :
 - CPU : 18 (+ 4 possible)
 - RAM (en Go) : 24 (+ 4 possible)
 - HDD (en Go) : 760 (+ 240 possible)
 - SSD (en Go) :  300 (+ 100 possible)


## Mail
Bonjour Mme Corset,

Je reviens vers vous suite aux précédents mails. Le projet dont je veux vous parler s'inscrit dans le cadre de mes études au CESI (Ecole d'informatique) mais il nous intéresse aussi à titre personnel en tant que futur responsable informatique. 

Nous travaillons sur une société de 350 personnes qui devrait assez vite atteindre les 450 personnes. Le SI de cette société comporte 3 personnes, il y a un site à Lille, c'est le siège social du groupe, un site à Dax, un à Annecy ainsi que 2 autres à Mâcon et Brest.
Le cloud nous permettrait normalement de gérer l'évolution de l'infrastructure et c'est pour ça qu'on qu'aujourd'hui nous nous intéressons aux différentes offres de cloud provider et d'infogérance. 

 - Nous serions intéressés par un devis sur votre offre de colocation dans un datacenter en France, l'entreprise est tournée vers l'écologie alors je souhaiterais aussi bien avoir les informations pour vos Datacenter que pour vos Datacenters neutres. Il y a 14 serveurs ce qui représente 20 vCPU et 64 vRAM pour une capacité de stockage de 6 To. 
 - Est-il possible de changer facilement ses caractéristiques selon le besoin ?
 - L'infra est hybride et nous avons des données sensibles que nous devons garder en interne et le reste dans le cloud, comment fonctionnez-vous pour ces infrastructures hybrides ?
 - Quelles sont vos modalités d'infogérances, votre offre est-elle une offre IAAS uniquement ou proposez-vous aussi d'autres services ?

Concernant votre offre FAI, sur votre site nous avons pu voir la carte de couverture réseau et vous ne semblez pas contacter tous les sites comme Dax et Mâcon. Est-ce vraiment le cas ? 
Si ce n'est pas le cas, quelles sont nos possibilités ? (Doit-on prendre un second FAI ou nous ne pouvons pas travailler ensemble ou autre ?)

 Si vous souhaitez plus d'informations nous serons ravis de vous les communiquer, je vous propose que nous nous recontactions par mail ou je me tiens à votre disposition pour convenir d'un rendez-vous en janvier par téléphone.

 Je vous remercie par avance d'avoir pris du temps pour nous répondre
Veuillez recevoir, Madame, Messieurs, l’expression de mes salutations les meilleures.

Jordan Turck

